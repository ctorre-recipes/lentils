Ingredients
===========

* Olive oil
* Garlic
* Onion

* Salt
* Ginger
* Turmeric
* Red chilli powder
* Curry powder

* Tomatoes
* Chillies
* Lentils

* Coconut milk
* (optional) Vegetable stock

About 1:1 of every spice
