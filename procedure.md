Procedure
=========

1. Put olive oil and garlic in a **frying pan**.
2. Chop onion and add it to pan with ginger and salt.
3. Chop tomatoes and chillies and add them to pan.
4. Add turmeric, red chilli powder, and curry powder.
5. Cook tomatoes until they reduce.
6. Add lentils, coconut milk, and water or (optional) vegetable stock.
7. Boil for 10 min or until the lentils are fully cooked.
